/*eslint-disable*/

import Vue from 'vue'
import Vuex from 'vuex'
import UserService from "./services/userService"
import BookingService from "./services/bookingService"

Vue.use(Vuex)

const userService = new UserService();
const bookingService = new BookingService();

export default new Vuex.Store({
    state: {
        isLoggedIn: false,
        token: null,
        login_result: null,
        login_errorMsg: null,
        register_result: null,
        register_errorMsg: null,
        booking_result: null,
        booking_errorMsg: null,
        book: false,
        bookingData: null,
        bookings: [],
        ticketData: null
    },
    getters: {
        bookingsLength: state => {
            return state.bookings.length
        }
    },
    mutations: {
        LOGIN_SUCCESS: (state, token) => {
            state.isLoggedIn = true;
            state.token = token;
            state.login_result = true;
        },
        LOGIN_FAIL: (state, error) => {
            state.login_result = false;
            state.login_errorMsg = error;
        },
        REGISTER_SUCCESS: (state) => {
            state.register_result = true;
        },
        REGISTER_FAIL: (state, error) => {
            state.register_result = false;
            state.register_errorMsg = error
        },
        BOOKING_SUCCESS: (state) => {
            state.booking_result = true;
        },
        BOOKING_FAILED: (state, msg) => {
            state.booking_result = false;
            state.booking_errorMsg = msg
        },
        LOGOUT: (state) => {
            state.token = null;
            state.isLoggedIn = false;
        },
        RESET_LOGIN_RESULT: (state) => {
            state.login_result = null;
        },
        RESET_REGISTER_RESULT: (state) => {
            state.register_result = null;
        },
        RESET_BOOKING_RESULT: (state) => {
            state.booking_result = null;
        },
        SET_LOGGEDIN: (state) => {
            state.isLoggedIn = true;
        },
        SET_BOOK: (state) => {
            state.book = true;
        },
        RESET_BOOK: (state) => {
            state.book = false;
        },
        SET_BOOKING: (state, booking) => {
            state.bookingData = booking;
        },
        RESET_BOOKING: (state) => {
            state.bookingData = null;
        },
        GET_BOOKINGS: (state, bookings) => {
            state.bookings = bookings;
        },
        SET_TICKET_DATA: (state, ticket) => {
            state.ticketData = ticket;
        }
    },
    actions: {
        checkLogin: async ({ commit }, user) => {
            await userService.loginUser(user).then(res => {
                localStorage.setItem("token", res.data.data.token);
                //commit('LOGIN_SUCCESS', { token: res.data.data.token, user: user })
                commit('LOGIN_SUCCESS', res.data.data.token)
            }).catch(err => {
                commit('LOGIN_FAIL', err.response.data.error)
            })
        },
        logoutUser: ({ commit }) => {
            localStorage.removeItem("token");
            commit('LOGOUT')
        },
        checkRegistration: async ({ commit }, register) => {
            await userService.registerUser(register).then(res => {
                commit('REGISTER_SUCCESS')
            }).catch(err => {
                commit('REGISTER_FAIL', err.response.data.error)
            })
        },
        bookFlight: async ({ commit }, booking) => {
            await bookingService.bookFlight(booking).then(res => {
                commit('BOOKING_SUCCESS')
            }).catch(err => {
                commit('BOOKING_FAILED', err.response.data.error)
            })
        },
        resetLoginResult: ({ commit }) => {
            commit('RESET_LOGIN_RESULT')
        },
        resetRegisterResult: ({ commit }) => {
            commit('RESET_REGISTER_RESULT')
        },
        resetBookingResult: ({ commit }) => {
            commit('RESET_BOOKING_RESULT')
        },
        setLoggedIn: ({ commit }) => {
            commit('SET_LOGGEDIN')
        },
        setBook: ({ commit }) => {
            commit('SET_BOOK')
        },
        resetBook: ({ commit }) => {
            commit('RESET_BOOK')
        },
        setBookingData: ({ commit }, booking) => {
            commit('SET_BOOKING', JSON.stringify(booking))
        },
        resetBookingData: ({ commit }) => {
            commit('RESET_BOOKING')
        },
        getBookings: async ({ commit }) => {
            await bookingService.getBookings().then(res => {
                commit('GET_BOOKINGS', res.data.data)
            }).catch(err => {
                console.log(err.response)
            })
        },
        setTicketData: ({ commit }, ticket) => {
            commit('SET_TICKET_DATA', JSON.stringify(ticket))
        }
    }
})