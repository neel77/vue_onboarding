/*eslint-disable*/
import axios from 'axios'

export default class BookingService {

    bookFlight(booking) {
        return axios.post('http://10.12.40.85:3000/flights/bookFlight', booking, {
            headers: {
                'Content-Type': 'application/json',
                'x-access-token': localStorage.getItem("token")
            }
        })
    }

    getBookings() {
        return axios.post('http://10.12.40.85:3000/flights/getBookingsData', {}, {
            headers: {
                'x-access-token': localStorage.getItem("token")
            }
        })
    }

    cancelFlight(id) {
        return axios.post('http://10.12.40.85:3000/flights/cancelFlight', id, {
            headers: {
                'x-access-token': localStorage.getItem("token")
            }
        })
    }
}