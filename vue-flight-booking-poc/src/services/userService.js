/*eslint-disable*/
import axios from 'axios'

export default class UserService {

    loginUser(user) {
        return axios.post('http://10.12.40.85:3000/users/login', user)
    }

    registerUser(register) {
        return axios.post('http://10.12.40.85:3000/users/register', register)
    }
}