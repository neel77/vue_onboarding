/*eslint-disable*/

import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import Route2 from './components/Route2'
import Route3 from './components/Route3'
import NestedRoute from './components/NestedRoute'
import View1 from './components/View1'
import AuthenticatedRoute from './components/AuthenticatedRoute'

Vue.config.productionTip = false
Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  routes: [
    { path: '/', redirect: { name: 'route1' } },
    { path: '/route1', name: 'route1', component: () => import('./components/Route1.vue'), meta: { requiresAuth: false } },
    {
      path: '/route2', component: Route2, alias: '/alias-route2', /*meta: {
        requiresAuth: true
      }*/
    },
    {
      path: '/route3/:id', component: Route3, children: [
        {
          path: 'nested-route', components: {
            default: NestedRoute,
            view: View1
          },
          props: { default: true, view: true },
        }
      ],
      props: true,
    },
    { path: '/authenticated', component: AuthenticatedRoute }
  ]
})

router.beforeEach((to, from, next) => {
  console.log(to.matched)
  if (to.meta.requiresAuth) next("/authenticated");
  else next();
});

router.afterEach((to, from) => {
  console.log(from);
  console.log(to)
})

new Vue({
  render: h => h(App),
  router
}).$mount('#app')





